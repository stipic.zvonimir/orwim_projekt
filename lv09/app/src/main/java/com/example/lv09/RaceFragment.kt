package com.example.lv09

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide


class RaceFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_race, container, false)
        var image = view.findViewById<ImageView>(R.id.trackImage)
        var name = view.findViewById<TextView>(R.id.trackName)
        var winner = view.findViewById<TextView>(R.id.raceWinner)
        var description = view.findViewById<TextView>(R.id.raceDescription)
        Glide.with(view.context).load(arguments?.getString("IMAGE").toString()).into(image)
        name.text = arguments?.getString("NAME").toString()
        winner.text = arguments?.getString("WINNER").toString()
        description.text = arguments?.getString("DESCRIPTION").toString()

        view.findViewById<Button>(R.id.button_back).setOnClickListener{
            val raceListFragment = RaceListFragment()
                val fragmentTransaction: FragmentTransaction =
                    requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragmentContainerView1, raceListFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        return view
    }
}