package com.example.lv09
@Suppress("unused", "unused")
data class Race(
    var id: String="",
    var name: String? = null,
    var trackPhotoUrl: String? = null,
    var winner: String? = null,
    var description: String? = null
)
