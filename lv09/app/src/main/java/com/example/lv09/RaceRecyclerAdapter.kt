package com.example.lv09

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class RaceRecyclerAdapter(
    private val items: ArrayList<Race>,
    private val listener: ContentListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var mListener: ItemListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            RecyclerView.ViewHolder {
        return RaceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_item, parent, false), mListener
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RaceViewHolder -> {
                holder.bind(position, items[position], listener)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun removeItem(index: Int) {
        items.removeAt(index)
        notifyItemRemoved(index)
        notifyItemRangeChanged(index, itemCount)
    }


    class RaceViewHolder(private val view: View, clickListener: ItemListener) : RecyclerView.ViewHolder(view) {
        init {
            itemView.setOnClickListener {
                clickListener.onItemClick(adapterPosition)
            }
        }
        private val trackImage = view.findViewById<ImageView>(R.id.trackImage)
        private val trackName = view.findViewById<TextView>(R.id.trackName)
        private val raceWinner = view.findViewById<TextView>(R.id.raceWinner)
        private val deleteButton = view.findViewById<ImageButton>(R.id.deleteButton)
        fun bind(index: Int, race: Race, listener: ContentListener) {
            Glide.with(view.context).load(race.trackPhotoUrl).into(trackImage)
            trackName.text = race.name
            raceWinner.text = race.winner
            deleteButton.setOnClickListener{
                listener.onItemButtonClick(index, race)
            }
        }
    }

    interface ItemListener {
        fun onItemClick(index: Int)
    }
    interface ContentListener {
        fun onItemButtonClick(
            index: Int, race: Race
        )
    }
    fun setOnItemClickListener(listener: ItemListener) {
        mListener = listener
    }
}
