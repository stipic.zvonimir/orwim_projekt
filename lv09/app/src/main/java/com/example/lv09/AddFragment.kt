package com.example.lv09

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.FragmentTransaction
import com.google.firebase.firestore.FirebaseFirestore

class AddFragment : Fragment() {
    private lateinit var firestore: FirebaseFirestore
    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add, container, false)
        firestore = FirebaseFirestore.getInstance()
        val name = view.findViewById<EditText>(R.id.editName)
        val trackPhotoUrl = view.findViewById<EditText>(R.id.editTrackPhotoUrl)
        val description = view.findViewById<EditText>(R.id.editDescription)
        val winner = view.findViewById<EditText>(R.id.editWinner)
        val saveButton = view.findViewById<ImageButton>(R.id.saveButton)
        saveButton.setOnClickListener {
            val race = Race(
                name = name.text.toString(),
                trackPhotoUrl = trackPhotoUrl.text.toString(),
                description = description.text.toString(),
                winner = winner.text.toString(),
            )
            firestore.collection("races").add(race)

            val fragmentTransaction: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction?.replace(R.id.fragmentContainerView1, RaceListFragment())
            fragmentTransaction?.commit()
        }
        view.findViewById<Button>(R.id.button_back1).setOnClickListener{
            val raceListFragment = RaceListFragment()
            val fragmentTransaction: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragmentContainerView1, raceListFragment)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }

        return view
    }
}