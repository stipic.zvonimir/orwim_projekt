package com.example.lv09

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import android.util.Log
import android.widget.Button
import android.widget.ImageButton


class RaceListFragment : Fragment(), RaceRecyclerAdapter.ContentListener {
    private val db = FirebaseFirestore.getInstance()
    private lateinit var recyclerAdapter: RaceRecyclerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val items: ArrayList<Race> =ArrayList()
        val view = inflater.inflate(R.layout.fragment_race_list, container, false)
        val recyclerView = view?.findViewById<RecyclerView>(R.id.recyclerView)

        db.collection("races")
            .get()
            .addOnSuccessListener { querySnapshot ->
                for (data in querySnapshot.documents) {
                    val race = Race(
                        id = data.id,
                        name = data.get("name").toString(),
                        winner = data.get("winner").toString(),
                        description = data.get("description").toString(),
                        trackPhotoUrl = data.get("trackPhotoUrl").toString()

                    )
                    items.add(race)
                }
                recyclerAdapter = RaceRecyclerAdapter(items, this@RaceListFragment)
                recyclerView?.apply {
                    layoutManager = LinearLayoutManager(requireContext())
                    adapter = recyclerAdapter
                }
                recyclerAdapter.setOnItemClickListener(listener = object :
                    RaceRecyclerAdapter.ItemListener {
                    override fun onItemClick(index: Int) {
                        val raceFragment = RaceFragment()
                        val bundle = Bundle()
                        bundle.putString("IMAGE", items[index].trackPhotoUrl)
                        bundle.putString("NAME", items[index].name)
                        bundle.putString("WINNER", items[index].winner)
                        bundle.putString("DESCRIPTION", items[index].description)
                        raceFragment.arguments = bundle

                        val fragmentTransaction: FragmentTransaction =
                            requireActivity().supportFragmentManager.beginTransaction()
                        fragmentTransaction.replace(R.id.fragmentContainerView1, raceFragment)/*double check*/
                        fragmentTransaction.addToBackStack(null)
                        fragmentTransaction.commit()
                    }
                })
                // Log the data fetched from Firebase
                Log.d("RaceListFragment", "it fetched someting, Data fetched from Firebase: $querySnapshot")
            }
            .addOnFailureListener {
                // Log the error
                Log.e("RaceListFragment", "Error fetching data from Firebase: $it")
                //end
            }
        val addButton = view.findViewById<ImageButton>(R.id.addButton)
        addButton.setOnClickListener {
            val fragmentTransaction: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragmentContainerView1, AddFragment())
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
        return view
    }

    override fun onItemButtonClick(index: Int, item: Race) {
        recyclerAdapter.removeItem(index)
        db.collection("races")
            .document(item.id)
            .delete()
    }
}